package com.social;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.social.dao.ActionPlanRepository;
import com.social.dao.CalendarUtilRepository;
import com.social.dao.IssuesLogsRepository;
import com.social.dao.Progress_Status_Repository;
import com.social.dao.ProjectTeamRepository;
import com.social.dao.ProjetRepository;
import com.social.dao.StatusDevRepository;
import com.social.dao.UserRepository;
import com.social.entities.CalendarUtil;
import com.social.entities.Projet;
import com.social.entities.User;

/**
 * 
 * @author kamal berriga
 *
 */
@SpringBootApplication
public class SpringBootSocialAuthApplication implements CommandLineRunner {
	@Autowired
	private Progress_Status_Repository progress_Status_Repository;
	@Autowired
	private ActionPlanRepository actionPlanRepository;
	@Autowired
	private IssuesLogsRepository issuesLogsRepository;
	@Autowired
	private ProjectTeamRepository projectTeamRepository;
	@Autowired
	private StatusDevRepository statusDevRepository;
	@Autowired
	private ProjetRepository projetRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private CalendarUtilRepository calendarUtilRespository;
	
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootSocialAuthApplication.class, args);
	}
	@Override
	public void run(String... arg0) throws Exception {
		this.calendarUtilRespository.deleteAllInBatch();
		Projet p = new Projet("domaine", "nomp", "ressource", new Date(), new Date());
		this.projetRepository.save(p);
		
		//User user = new User("user1", "tttt", "aa");
		//User user2 = new User("user2", "llll", "bb");
		//User user3 = new User("user3", "mmmm", "cc");
		
		//this.userRepository.save(user);
		//this.userRepository.save(user2);
		//this.userRepository.save(user3);
		
		List<User> listUser =  this.userRepository.findAll();
		System.out.println("List of user");
		listUser.forEach(u->System.out.println(u));
		
		/*List<Projet> lsProjets= this.projetRepository.findAll();
		//lsProjets.forEach(pr-> System.out.println(pr.getDomaine()));
		//Projet projet = lsProjets.get(0);
		List<User> users = this.userRepository.findAll();
		User user = users.get(0);
		CalendarUtil calendarUtil = this.calendarUtilRespository.save(new CalendarUtil());
		calendarUtil.setProjet(projet);
		calendarUtil.setUser(user);

		Calendar cal = Calendar.getInstance();
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd ");
		Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		System.out.println(dateFormat.format(date));
		calendarUtil.setNumberOfDays(cal.get(Calendar.DAY_OF_MONTH));
		calendarUtil.setMonth(cal.get(Calendar.MONTH)+1+"");
		calendarUtil.setYear(cal.get(Calendar.YEAR));
		this.calendarUtilRespository.save(calendarUtil);
		//
*/		//Projet projet=new Projet( "domaine", "nomp", "ressource", "datedebut", "datefin", listactivite, listuser);
		//User user=new User(id, username, password, role, fullName, listprojet)
		
		
	}
}

