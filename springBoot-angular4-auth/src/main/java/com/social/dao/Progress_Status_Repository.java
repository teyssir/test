package com.social.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.social.entities.Progress_Status;

public interface Progress_Status_Repository extends JpaRepository<Progress_Status, Integer>{

}
