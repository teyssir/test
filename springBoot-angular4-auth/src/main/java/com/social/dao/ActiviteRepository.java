package com.social.dao;

import org.springframework.data.jpa.repository.JpaRepository;


import com.social.entities.Activite;

public interface ActiviteRepository extends JpaRepository<Activite, Integer> {

}
