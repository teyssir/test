package com.social.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.social.entities.StatusDev;

public interface StatusDevRepository extends JpaRepository<StatusDev, Integer> {

}
