package com.social.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.social.entities.IssuesLogs;

public interface IssuesLogsRepository extends JpaRepository<IssuesLogs, Integer>{

}
