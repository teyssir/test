package com.social.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.social.entities.Projet;

public interface ProjetRepository extends JpaRepository<Projet, Integer> {
	
	//public List<Projet> findBy(Long id);

}
