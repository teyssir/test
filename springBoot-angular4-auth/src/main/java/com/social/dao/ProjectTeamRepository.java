package com.social.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.social.entities.ProjectTeam;

public interface ProjectTeamRepository extends JpaRepository<ProjectTeam, Integer>{

}
 