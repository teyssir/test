package com.social.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.social.entities.ActionPlan;

public interface ActionPlanRepository extends JpaRepository<ActionPlan, Integer> {

}
