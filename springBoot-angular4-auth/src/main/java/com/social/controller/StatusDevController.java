package com.social.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.social.dao.StatusDevRepository;
import java.util.List;
import com.social.entities.StatusDev;


@RestController
@RequestMapping("/sd")
@CrossOrigin(origins="http://localhost:4200",allowedHeaders="*")
public class StatusDevController {
	
	@Autowired
	
	private StatusDevRepository statusDevRepository;
	
	@GetMapping("/statusdevs")
	
	public List<StatusDev> getStatusDevs(){
		return statusDevRepository.findAll();
		
	}
	
	@GetMapping("/statusdev/{bloque}")
	public StatusDev getStatusDev(@PathVariable Integer bloque) {
		return statusDevRepository.findOne(bloque); 
	}
	
	@DeleteMapping("/statusdev/{bloque}")
	public boolean deleteStatusDev(@PathVariable Integer bloque) {
		 statusDevRepository.delete(bloque); 
		 return true;
	}
	
	@PutMapping("/statusdev")
	public StatusDev updateStatusDevs(@RequestBody StatusDev statusdev  ) {
		return statusDevRepository.save(statusdev); 
	}
	
	@PostMapping("/statusdev")
	public StatusDev createStatusDevs(@RequestBody StatusDev statusdev  ) {
		return statusDevRepository.save(statusdev); 
	}
	
	

}
