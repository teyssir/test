package com.social.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.social.dao.ActionPlanRepository;
import java.util.List;
import com.social.entities.ActionPlan;
@RestController
@RequestMapping("/ap")
@CrossOrigin(origins="http://localhost:4200",allowedHeaders="*")

public class ActionPlanController {
	
	@Autowired
	private ActionPlanRepository actionPlanRepository;
	
	@GetMapping("/actionplans")
	public List<ActionPlan> getActionPlans(){
	return actionPlanRepository.findAll();	
	}
	@GetMapping("/actionplan/{id} ")
	public ActionPlan getActionPlan(@PathVariable Integer id) {
	
		return actionPlanRepository.findOne(id);
	}
	
	@DeleteMapping("/actionplan/{id}")
	public boolean deleteActionPlan(@PathVariable Integer id) {
		actionPlanRepository.delete(id); 
		 return true;
	}
	
	@PutMapping("/actionplan")
	public ActionPlan updateActionPlan (@RequestBody ActionPlan actionplan) {
		return actionPlanRepository.save(actionplan) ;
	}
	@PostMapping("/actionplan")
	public ActionPlan createActionPlan (@RequestBody ActionPlan actionplan) {
		return actionPlanRepository.save(actionplan) ;
	}
	
}
