package com.social.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.social.dao.ProjetRepository;
import com.social.dao.UserRepository;
import com.social.entities.Projet;

@RestController
@RequestMapping("/pr")
@CrossOrigin(origins="http://localhost:4200",allowedHeaders="*")


public class ProjetController {
	@Autowired
	
	private ProjetRepository projetrepository;
	
	@GetMapping("/projets")
	
	public List<Projet> getProjets(){
	return projetrepository.findAll();	
	}
	
	@GetMapping("/projet/{idP} ")
	/*public List<Projet> getProjetUser(@PathVariable Long id){
		 return projetrepository.findByListuser(id);
		 }
	*/
	
	public Projet getProjet(@PathVariable Integer idP) {
	
		return projetrepository.findOne(idP);
	}
	
	
	@DeleteMapping("/projet/{idP}")
	public boolean deleteProjet(@PathVariable Integer idP) {
		projetrepository.delete(idP); 
		 return true;
	}
	
	@PutMapping("/projet")
	public Projet updateProjet(@RequestBody Projet projet  ) {
		return projetrepository.save(projet); 
	}
      @PostMapping("/projet")
	public Projet createProjet(@RequestBody Projet projet  ) {
		return projetrepository.save(projet); 
	}
	
	

}
