package com.social.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.social.dao.ActiviteRepository;
import com.social.entities.Activite;


@RestController
@RequestMapping("/act")
@CrossOrigin(origins="http://localhost:4200",allowedHeaders="*")

public class ActiviteController {
	
	@Autowired
	private ActiviteRepository activiteRepository;
	
	@GetMapping("/activites")
	public List<Activite> getActivites(){
	return activiteRepository.findAll();	
	}
	@GetMapping("/activite/{idactivite} ")
	public Activite getActivite(@PathVariable Integer idactivite) {
	
		return activiteRepository.findOne(idactivite);
	}
	
	@DeleteMapping("/activite/{idactivite}")
	public boolean deleteActivite (@PathVariable Integer idactivite) {
		activiteRepository.delete(idactivite); 
		 return true;
	}
	
	@PutMapping("/activite")
	public Activite updateActivite  (@RequestBody Activite activite) {
		return activiteRepository.save(activite) ;
	}
	@PostMapping("/activite")
	public Activite createActivite (@RequestBody Activite activite) {
		return activiteRepository.save(activite) ;
	}
	
	

}
