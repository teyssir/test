package com.social.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.social.dao.IssuesLogsRepository;
import java.util.List;
import com.social.entities.IssuesLogs;


@RestController
@RequestMapping("/is")
@CrossOrigin(origins="http://localhost:4200",allowedHeaders="*")

public class IssuesLogsController {
	@Autowired
	private  IssuesLogsRepository issuesLogsRepository;
	
	@GetMapping("/issueslogss")
	public List<IssuesLogs> getIssuesLogss(){
		return issuesLogsRepository.findAll(); 
	}
	@GetMapping("/issueslogs/{id}")
	public IssuesLogs getIssuesLogs(@PathVariable Integer id) {
		return issuesLogsRepository.findOne(id);
	}
	
	@DeleteMapping("/issueslogs/{id}")
	public boolean deleteIssuesLogs(@PathVariable Integer id) {
		issuesLogsRepository.delete(id); 
		 return true;
	}
	
	@PutMapping("/issueslogs")
	public IssuesLogs updateIssuesLogs (@RequestBody IssuesLogs issueslogs  ) {
		return issuesLogsRepository.save(issueslogs); 
	}
      @PostMapping("/issueslogs")
	public IssuesLogs createIssuesLogs (@RequestBody IssuesLogs issueslogs  ) {
		return issuesLogsRepository.save(issueslogs); 
	}

}
