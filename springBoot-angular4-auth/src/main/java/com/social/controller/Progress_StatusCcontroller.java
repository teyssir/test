package com.social.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.social.dao.Progress_Status_Repository;
import com.social.entities.Progress_Status;



import java.util.List;
@RestController
@RequestMapping("/api")
@CrossOrigin(origins="http://localhost:4200",allowedHeaders="*")
public class Progress_StatusCcontroller {
	@Autowired
	private Progress_Status_Repository progress_Status_Repository;
	@GetMapping("/progress")
	public List <Progress_Status> getProgress() {
		return progress_Status_Repository.findAll(); 
	}
	@GetMapping("/progres/{lot}")
	public Progress_Status getProgres(@PathVariable Integer lot) {
		return progress_Status_Repository.findOne(lot); 
	}
	@DeleteMapping("/progres/{lot}")
	public boolean deleteProgres(@PathVariable Integer lot) {
		progress_Status_Repository.delete(lot);
		return true;
	}
	
	@PutMapping("/progres")
	public Progress_Status updateProgres(@RequestBody Progress_Status progress_Status) {
		return progress_Status_Repository.save(progress_Status); 
 }
     @PostMapping("/progres")
	public Progress_Status createProgres(@RequestBody Progress_Status progress_Status) {
		return progress_Status_Repository.save(progress_Status); 

}
	
	
	
	
}
