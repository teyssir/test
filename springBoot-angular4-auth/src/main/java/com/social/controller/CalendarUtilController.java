package com.social.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.social.dao.CalendarUtilRepository;
import com.social.entities.CalendarUtil;
import com.social.entities.Projet;
@RestController

public class CalendarUtilController {
	@Autowired 
	CalendarUtilRepository CR;
	@GetMapping("/calendar")
	public CalendarUtil getData(){
		
		return new CalendarUtil();
		}
	@GetMapping("/calendarall")
	public List< CalendarUtil> getall(){
		
		return CR.findAll();
		}
   @PostMapping("/calendar")
	public CalendarUtil createCalendarUtil(@RequestBody CalendarUtil calendarutil  ) {
		return CR.save(calendarutil); 
	}
	
	
	

}
