package com.social.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.social.dao.ProjectTeamRepository;
import com.social.entities.ProjectTeam;
@RestController
@RequestMapping("/pt")
@CrossOrigin(origins="http://localhost:4200",allowedHeaders="*")

public class ProjectTeamController {
	@Autowired
	private ProjectTeamRepository projectTeamRepository;
	
	@GetMapping("/projectteams")
	public List <ProjectTeam> getProjectteams(){
		return projectTeamRepository.findAll();
		
	}
	
	@GetMapping("/projectteam/{id} ")
	public ProjectTeam getProjectteam(@PathVariable Integer id) {
	return projectTeamRepository.findOne(id); 	
	}
	@DeleteMapping("/projectteam/{id}")
	public boolean deleteProjectTeam(@PathVariable Integer id) {
		projectTeamRepository.delete(id); 
		 return true;
	}
	
	@PutMapping("/projectteam")
	public ProjectTeam updateProjectTeam(@RequestBody ProjectTeam projectteam   ) {
		return projectTeamRepository.save(projectteam); 
	}
	
	@PostMapping("/projectteam")
	public ProjectTeam createProjectTeam(@RequestBody ProjectTeam projectteam   ) {
		return projectTeamRepository.save(projectteam); 
	}
	

}
