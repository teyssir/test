package com.social.entities;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class ActionPlan {
	@Id
	@GeneratedValue 
 
	private Integer id;
	private String identified_by;
	private Date date_debut;
	private Date date_fin;
	private String statut_progression;
	private String responsable;
	private String commentaires;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getIdentified_by() {
		return identified_by;
	}
	public void setIdentified_by(String identified_by) {
		this.identified_by = identified_by;
	}
	public Date getDate_debut() {
		return date_debut;
	}
	public void setDate_debut(Date date_debut) {
		this.date_debut = date_debut;
	}
	public Date getDate_fin() {
		return date_fin;
	}
	public void setDate_fin(Date date_fin) {
		this.date_fin = date_fin;
	}
	public String getStatut_progression() {
		return statut_progression;
	}
	public void setStatut_progression(String statut_progression) {
		this.statut_progression = statut_progression;
	}
	public String getResponsable() {
		return responsable;
	}
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}
	public String getCommentaires() {
		return commentaires;
	}
	public void setCommentaires(String commentaires) {
		this.commentaires = commentaires;
	}

	
	public ActionPlan(String identified_by, Date date_debut, Date date_fin, String statut_progression,
			String responsable, String commentaires) {
		
		this.identified_by = identified_by;
		this.date_debut = date_debut;
		this.date_fin = date_fin;
		this.statut_progression = statut_progression;
		this.responsable = responsable;
		this.commentaires = commentaires;
	}
	public ActionPlan() {
		
	}
	@Override
	public String toString() {
		return "ActionPlan [id=" + id + ", identified_by=" + identified_by + ", date_debut=" + date_debut
				+ ", date_fin=" + date_fin + ", statut_progression=" + statut_progression + ", responsable="
				+ responsable + ", commentaires=" + commentaires + "]";
	}

	
	

}
