package com.social.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

//@Entity
public class Event {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	int id;
	long nb;
	

	@ManyToOne	
	@JoinColumn(name="eventcalendar" ,referencedColumnName="id" )
	@JsonIgnore
	CalendarUtil calendarutil;



	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public long getText() {
		return nb;
	}


	public void setText(long text) {
		this.nb = text;
	}


	public CalendarUtil getCalendarutil() {
		return calendarutil;
	}


	public void setCalendarutil(CalendarUtil calendarutil) {
		this.calendarutil = calendarutil;
	}
	
	

}
