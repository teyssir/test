package com.social.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity
public class StatusDev {
	
	@Id
	@GeneratedValue 
	
	private Integer bloque;
	private Integer ordredepriorite;
	private String produit;
	private String extraction;
	private String analyse;
	private String migration;
	private String fichierinput;
	private String fichieroutput;
	private String test;
	private String ouwner;
	private Integer chargeanalyse;
	private Integer chargedev;
	private Integer tests ;
	private Integer total;
	private Integer consomme;
	private Integer restant;
	private Integer rae;
	private String deliveryinterne;
	private Integer sprintdemande;
	private Integer sprintpropose;
	private String deliveryauclient;
	private String commentaires;
	public Integer getBloque() {
		return bloque;
	}
	public void setBloque(Integer bloque) {
		this.bloque = bloque;
	}
	public Integer getOrdredepriorite() {
		return ordredepriorite;
	}
	public void setOrdredepriorite(Integer ordredepriorite) {
		this.ordredepriorite = ordredepriorite;
	}

	public String getProduit() {
		return produit;
	}
	public void setProduit(String produit) {
		this.produit = produit;
	}
	public String getExtraction() {
		return extraction;
	}
	public void setExtraction(String extraction) {
		this.extraction = extraction;
	}
	public String getAnalyse() {
		return analyse;
	}
	public void setAnalyse(String analyse) {
		this.analyse = analyse;
	}
	public String getMigration() {
		return migration;
	}
	public void setMigration(String migration) {
		this.migration = migration;
	}
	public String getFichierinput() {
		return fichierinput;
	}
	public void setFichierinput(String fichierinput) {
		this.fichierinput = fichierinput;
	}
	public String getFichieroutput() {
		return fichieroutput;
	}
	public void setFichieroutput(String fichieroutput) {
		this.fichieroutput = fichieroutput;
	}
	public String getTest() {
		return test;
	}
	public void setTest(String test) {
		this.test = test;
	}
	public String getOuwner() {
		return ouwner;
	}
	public void setOuwner(String ouwner) {
		this.ouwner = ouwner;
	}
	public Integer getChargeanalyse() {
		return chargeanalyse;
	}
	public void setChargeanalyse(Integer chargeanalyse) {
		this.chargeanalyse = chargeanalyse;
	}
	public Integer getChargedev() {
		return chargedev;
	}
	public void setChargedev(Integer chargedev) {
		this.chargedev = chargedev;
	}
	public Integer getTests() {
		return tests;
	}
	public void setTests(Integer tests) {
		this.tests = tests;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public Integer getConsomme() {
		return consomme;
	}
	public void setConsomme(Integer consomme) {
		this.consomme = consomme;
	}
	public Integer getRestant() {
		return restant;
	}
	public void setRestant(Integer restant) {
		this.restant = restant;
	}
	public Integer getRae() {
		return rae;
	}
	public void setRae(Integer rae) {
		this.rae = rae;
	}
	public String getDeliveryinterne() {
		return deliveryinterne;
	}
	public void setDeliveryinterne(String deliveryinterne) {
		this.deliveryinterne = deliveryinterne;
	}
	public Integer getSprintdemande() {
		return sprintdemande;
	}
	public void setSprintdemande(Integer sprintdemande) {
		this.sprintdemande = sprintdemande;
	}
	public Integer getSprintpropose() {
		return sprintpropose;
	}
	public void setSprintpropose(Integer sprintpropose) {
		this.sprintpropose = sprintpropose;
	}
	public String getDeliveryauclient() {
		return deliveryauclient;
	}
	public void setDeliveryauclient(String deliveryauclient) {
		this.deliveryauclient = deliveryauclient;
	}
	public String getCommentaires() {
		return commentaires;
	}
	public void setCommentaires(String commentaires) {
		this.commentaires = commentaires;
	}
	public StatusDev(Integer bloque, Integer ordredepriorite, String produit, String extraction, String analyse,
			String migration, String fichierinput, String fichieroutput, String test, String ouwner,
			Integer chargeanalyse, Integer chargedev, Integer tests, Integer total, Integer consomme, Integer restant,
			Integer rae, String deliveryinterne, Integer sprintdemande, Integer sprintpropose, String deliveryauclient,
			String commentaires) {
		super();
		this.bloque = bloque;
		this.ordredepriorite = ordredepriorite;
		this.produit = produit;
		this.extraction = extraction;
		this.analyse = analyse;
		this.migration = migration;
		this.fichierinput = fichierinput;
		this.fichieroutput = fichieroutput;
		this.test = test;
		this.ouwner = ouwner;
		this.chargeanalyse = chargeanalyse;
		this.chargedev = chargedev;
		this.tests = tests;
		this.total = total;
		this.consomme = consomme;
		this.restant = restant;
		this.rae = rae;
		this.deliveryinterne = deliveryinterne;
		this.sprintdemande = sprintdemande;
		this.sprintpropose = sprintpropose;
		this.deliveryauclient = deliveryauclient;
		this.commentaires = commentaires;
	}
	public StatusDev() {
		super();
	}
	@Override
	public String toString() {
		return "StatusDev [bloque=" + bloque + ", ordredepriorite=" + ordredepriorite + ", Produit=" + produit
				+ ", extraction=" + extraction + ", analyse=" + analyse + ", migration=" + migration + ", fichierinput="
				+ fichierinput + ", fichieroutput=" + fichieroutput + ", test=" + test + ", ouwner=" + ouwner
				+ ", chargeanalyse=" + chargeanalyse + ", chargedev=" + chargedev + ", tests=" + tests + ", total="
				+ total + ", consomme=" + consomme + ", restant=" + restant + ", rae=" + rae + ", deliveryinterne="
				+ deliveryinterne + ", sprintdemande=" + sprintdemande + ", sprintpropose=" + sprintpropose
				+ ", deliveryauclient=" + deliveryauclient + ", commentaires=" + commentaires + "]";
	}
	
	
	

}
