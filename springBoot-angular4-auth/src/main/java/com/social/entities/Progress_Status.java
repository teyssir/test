package com.social.entities;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity

public class Progress_Status {
	@Id
	@GeneratedValue 
	private Integer lot;
	private String taches;
	private Date date_debut;
	private  Date date_fin;
	private String statut_progression;
	private String responsable;
	private String commentaires;
	public Integer getLot() {
		return lot;
	}
	public void setLot(Integer lot) {
		this.lot = lot;
	}
	public String getTaches() {
		return taches;
	}
	public void setTaches(String taches) {
		this.taches = taches;
	}
	public Date getDate_debut() {
		return date_debut;
	}
	public void setDate_debut(Date date_debut) {
		this.date_debut = date_debut;
	}
	public Date getDate_fin() {
		return date_fin;
	}
	public void setDate_fin(Date date_fin) {
		this.date_fin = date_fin;
	}
	public String getStatut_progression() {
		return statut_progression;
	}
	public void setStatut_progression(String statut_progression) {
		this.statut_progression = statut_progression;
	}
	public String getResponsable() {
		return responsable;
	}
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}
	public String getCommentaires() {
		return commentaires;
	}
	public void setCommentaires(String commentaires) {
		this.commentaires = commentaires;
	}
	public Progress_Status(Integer lot, String taches, Date date_debut, Date date_fin, String statut_progression,
			String responsable, String commentaires) {
	
		this.lot = lot;
		this.taches = taches;
		this.date_debut = date_debut;
		this.date_fin = date_fin;
		this.statut_progression = statut_progression;
		this.responsable = responsable;
		this.commentaires = commentaires;
	}
	public Progress_Status() {
	
	}
	
}



