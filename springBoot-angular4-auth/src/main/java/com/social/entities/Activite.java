package com.social.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity

public class Activite {
	@Id
	@GeneratedValue 
 
	private Integer idactivite;
	private String nom;
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="idProjet" ,referencedColumnName="idP" )
	

	private Projet projet;
	
	public Integer getIdactivite() {
		return idactivite;
	}
	public void setIdactivite(Integer idactivite) {
		this.idactivite = idactivite;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public Projet getProjet() {
		return projet;
	}
	public void setProjet(Projet projet) {
		this.projet = projet;
	}
	public Activite(Integer idactivite, String nom, Projet projet) {
	
		this.idactivite = idactivite;
		this.nom = nom;
		this.projet = projet;
	}
	public Activite() {
		
	}
	@Override
	public String toString() {
		return "Activite [idactivite=" + idactivite + ", nom=" + nom + ", projet=" + projet + "]";
	}
	
	
	

	
	
	

}
