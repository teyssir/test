package com.social.entities;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
public class CalendarUtil {
	@Id
	@GeneratedValue
	private int id;
	private int valeur;
	private String month;
	private int numberOfDays;
	private int year;
	private int dayofmonth;
	
	public int getDayofmonth() {
		return dayofmonth;
	}
	public void setDayofmonth(int dayofmonth) {
		this.dayofmonth = dayofmonth;
	}


	/*
	@OneToMany(mappedBy="calendarutil")

	private List<Event> eventlist;
	*/
	@JsonProperty(access = Access.WRITE_ONLY) 
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="user_id")
	private User user;
	
	@JsonProperty(access = Access.WRITE_ONLY) 
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="projet_idP")
	private Projet projet;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Projet getProjet() {
		return projet;
	}
	public void setProjet(Projet projet) {
		this.projet = projet;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public int getNumberOfDays() {
		return numberOfDays;
	}
	public void setNumberOfDays(int numberOfDays) {
		this.numberOfDays = numberOfDays;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getValeur() {
		return valeur;
	}
	public void setValeur(int valeur) {
		this.valeur = valeur;
	}

	

	public CalendarUtil(int id, int valeur, String month, int numberOfDays, int year, int dayofmonth, User user,
			Projet projet) {
		super();
		this.id = id;
		this.valeur = valeur;
		this.month = month;
		this.numberOfDays = numberOfDays;
		this.year = year;
		this.dayofmonth = dayofmonth;
		this.user = user;
		this.projet = projet;
	}
	public CalendarUtil() {
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd ");
		Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		System.out.println(dateFormat.format(date));
	
		
		Calendar cal=Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		this.month=getMonthString(cal.get(Calendar.MONTH));
		
		this.numberOfDays=cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		
		this.year=cal.get(Calendar.YEAR);
		this.dayofmonth=cal.get(Calendar.DAY_OF_MONTH);
		
	
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	

	private String getMonthString(int monthNumber) {
		switch(monthNumber) {
		case 0 : return ("Janvier");
		
		case 1 : return("Février");
	
		case 2 : return("Mars");
		
		case 3 : return("Avril");
	
		case 4 : return("Mai");
		
		case 5 : return("Juin");
		
		case 6 : return("Juillet");
		
		case 7 : return("Août");
	
		case 8 : return("Septembre");
		
		case 9: return("Octobre");
		
		case 10: return("Novembre");
	
		case 11 : return("Décembre");
	
		
		}
		return month;
	}
	
}
