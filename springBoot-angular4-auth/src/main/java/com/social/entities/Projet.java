package com.social.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import org.hibernate.annotations.CascadeType;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;




@Entity
public class Projet {
	@Id
	@GeneratedValue 
	private Integer idP;
	private String domaine;
	private String nomp;

	private String tache;
	private Date datedebut;
	private Date datefin;
	
//	@JsonProperty(access = Access.WRITE_ONLY) 
//	@OneToMany(mappedBy="projet",fetch=FetchType.EAGER)
//	private List<Activite> listactivite ;
	
	@JsonIgnore
	@ManyToMany
    private List<User> listuser ;
	//@JsonProperty(access = Access.WRITE_ONLY) 
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="projet")
	private List<CalendarUtil> calendarUtils;

	public List<CalendarUtil> getCalendarUtils() {
		return calendarUtils;
	}


	public void setCalendarUtils(List<CalendarUtil> calendarUtils) {
		this.calendarUtils = calendarUtils;
	}


	public Integer getIdP() {
		return idP;
	}


	public void setIdP(Integer idP) {
		this.idP = idP;
	}


	public String getDomaine() {
		return domaine;
	}


	public void setDomaine(String domaine) {
		this.domaine = domaine;
	}


	public String getNomp() {
		return nomp;
	}


	public void setNomp(String nomp) {
		this.nomp = nomp;
	}


	public String getTache() {
		return tache;
	}


	public void setTache(String tache) {
		this.tache = tache;
	}


	public Date getDatedebut() {
		return datedebut;
	}


	public void setDatedebut(Date datedebut) {
		this.datedebut = datedebut;
	}


	public Date getDatefin() {
		return datefin;
	}


	public void setDatefin(Date datefin) {
		this.datefin = datefin;
	}


//	public List<Activite> getListactivite() {
//		return listactivite;
//	}


//	public void setListactivite(List<Activite> listactivite) {
//		this.listactivite = listactivite;
//	}



    public Projet(Integer idP, String domaine, String nomp, String tache, Date datedebut, Date datefin)
			//List<Activite> listactivite)
    		{
	
		this.idP = idP;
		this.domaine = domaine;
		this.nomp = nomp;
		this.tache = tache;
		this.datedebut = datedebut;
		this.datefin = datefin;
//	this.listactivite = listactivite;
		
	}
    public Projet(String domaine, String nomp, String tache, Date datedebut, Date datefin) {
	
		this.domaine = domaine;
		this.nomp = nomp;
		this.tache = tache;
		this.datedebut = datedebut;
		this.datefin = datefin;
	
		
	}



	public Projet() {
		
	}
	
	

}
