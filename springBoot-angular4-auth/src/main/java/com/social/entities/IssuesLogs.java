package com.social.entities;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity

public class IssuesLogs {
	@Id
	@GeneratedValue 
	private Integer id;
	private Date creation_date;
	private String Produit;
	private String issues_description;
	private String priority;
	private String reported_by;
	private String status;
	private Date end_date;
	private String comments;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Date getCreation_date() {
		return creation_date;
	}
	public void setCreation_date(Date creation_date) {
		this.creation_date = creation_date;
	}
	public String getProduit() {
		return Produit;
	}
	public void setProduit(String produit) {
		Produit = produit;
	}
	public String getIssues_description() {
		return issues_description;
	}
	public void setIssues_description(String issues_description) {
		this.issues_description = issues_description;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getReported_by() {
		return reported_by;
	}
	public void setReported_by(String reported_by) {
		this.reported_by = reported_by;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getEnd_date() {
		return end_date;
	}
	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public IssuesLogs(Integer id,Date creation_date, String produit, String issues_description, String priority,
			String reported_by, String status, Date end_date, String comments) {
	
		this.id = id;
		this.creation_date = creation_date;
		Produit = produit;
		this.issues_description = issues_description;
		this.priority = priority;
		this.reported_by = reported_by;
		this.status = status;
		this.end_date = end_date;
		this.comments = comments;
	}
	public IssuesLogs() {
		
	}
	@Override
	public String toString() {
		return "IssuesLogs [id=" + id + ", creation_date=" + creation_date + ", Produit=" + Produit
				+ ", issues_description=" + issues_description + ", priority=" + priority + ", reported_by="
				+ reported_by + ", status=" + status + ", end_date=" + end_date + ", comments=" + comments + "]";
	}
	
	
	

}
