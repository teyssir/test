package com.social.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class ProjectTeam {
	@Id
	@GeneratedValue 
	private Integer id;

	private String nom;
	private String mob;
	private String sopra_hr_email;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getMob() {
		return mob;
	}
	public void setMob(String mob) {
		this.mob = mob;
	}
	public String getSopra_hr_email() {
		return sopra_hr_email;
	}
	public void setSopra_hr_email(String sopra_hr_email) {
		this.sopra_hr_email = sopra_hr_email;
	}
	public ProjectTeam(String nom, String mob, String sopra_hr_email) {
	
		this.nom = nom;
		this.mob = mob;
		this.sopra_hr_email = sopra_hr_email;
	}
	public ProjectTeam() {

	}
	@Override
	public String toString() {
		return "ProjectTeam [id=" + id + ", nom=" + nom + ", mob=" + mob + ", sopra_hr_email=" + sopra_hr_email + "]";
	}
	

	
	
	

}
